// join 관련 함수들
        
// materialize.toast를 사용하려면 이 파일을 가져와야 할 것 같은데...
        // <script src="./materialize.js"></script>


        var joining = false;
        var database = firebase.database();
        var email;
        var password;
        var passwordConfirm;
        var strToday;
        var boollogin = false;
        // var createWallet = require('createWallet');


        function zeroPad(nr, base) {
            var len = (String(base).length - String(nr).length) + 1;
            return len > 0 ? new Array(len).join('0') + nr : nr;
        }

        $(function() {
            var xmlHttp;
            var st
            var st = srvTime();
            var today = new Date(st);
            strToday = today.getFullYear() + "" + zeroPad((today.getMonth() + 1), 10) + "" + today.getDate() + "_" + today.getHours() + "" + today.getMinutes();
        });
 
        function controllInputText(b) {
            $('#email').prop('disabled', !b);
            $('#password').prop('disabled', !b);
            $('#password_confirm').prop('disabled', !b);

            // if (!b) {
            //     $('.loader-parent').attr('style', 'visibility:visible;');
            // } else {
            //     $('.loader-parent').attr('style', 'visibility: collapse;');
            // }
        }

// register 버튼 (id="join") click하면...
// validate를 한 후
        $('#join').click(function validate() {
            controllInputText(false);

            email = $('#email').val();
            password = $('#password').val();
            passwordConfirm = $('#password_confirm').val();

            var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            /* validation check */
            if (!regEmail.test(email)) {
                controllInputText(true);
                Materialize.toast('Invalid Email Address', 2000);
                $('#email').focus();
                return;
            } else if (password.length < 6) {
                controllInputText(true);
                Materialize.toast('Password must be at least 6 characters', 2000);
                $('#password').focus();
                return;
            } else if (password != passwordConfirm) {
                controllInputText(true);
                Materialize.toast('Password and password confirm are different', 2000);
                $('#password_confirm').focus();
                return;
            }
            /* validation check end */

            Join();

        });

        // 회원 가입 절차를 진행한다
        function Join() {
            joining = true;
            console.log("joining " + joining + email);
            firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode == "auth/email-already-in-use" ||
                    errorCode == "auth/invalid-email" ||
                    errorCode == "auth/operation-not-allowed" ||
                    errorCode == "auth/weak-password") {
                    // Materialize.toast('Failed register. Please check your Email address. Your account may already exist.', 4000);
                    controllInputText(true);
                    $('#email').focus();
                    console.log("join fail " + errorCode + "|" + errorMessage);
                }
                // ...
            });
            // createWallet();
        }

// login 버튼 (id="goLogin") click하면 login 페이지로 이동한다.
        $('#goLogin').click(function goLogin() {
            location.href = "./login.html";
        });


        $('#goRegister').click(function goRegister() {
            location.href = "./register.html";
        });


        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                console.log("get user nojoin " + user.email);
                // 처음 가입하는 회원이라면...
                if (joining) {
                    console.log("get user join " + user.email);
                    writeUserData();
                } else {
                    firebase.auth().signOut().then(function() {
                        // Sign-out successful.
                    }).catch(function(error) {
                        // An error happened.
                    });
                }
                
                // 로그인한 상태라면 여기로 이동...
                location.href = "./main.html";


            } else {
                // No user is signed in.
            }
        });

// 신규가입시 입력한 email, pw 및 기타 데이터까지 DB에 저장한다.
// uid는 user.uid에 firebase system에서 자동으로 생성해 놓은 데이터가 있음

        function writeUserData() {
            var user = firebase.auth().currentUser;
            var name, email, photoUrl, uid, emailVerified, agent, country;

            var agentID = $('#agentID').val();
            var country = $('#country').val();
            console.log(country);
            email = user.email;
            uid = user.uid; 
            // The user's ID, unique to the Firebase project. Do NOT use
            // this value to authenticate with your backend server, if
            // you have one. Use User.getToken() instead.
            firebase.database().ref('users/' + uid).set({
                uid: uid,
                email: email,
                joindate: strToday,
                agentID: agentID,
                country: country
            });
            console.log(email, '- user data was written!')

             location.href = "../main.html";
        }

// 무슨 내용인지....
        function srvTime() {
            if (window.XMLHttpRequest) { //분기하지 않으면 IE에서만 작동된다.
                xmlHttp = new XMLHttpRequest(); // IE 7.0 이상, 크롬, 파이어폭스 등
                xmlHttp.open('HEAD', window.location.href.toString(), false);
                xmlHttp.setRequestHeader("Content-Type", "text/html");
                xmlHttp.send('');
                return xmlHttp.getResponseHeader("Date");
            } else if (window.ActiveXObject) {
                xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
                xmlHttp.open('HEAD', window.location.href.toString(), false);
                xmlHttp.setRequestHeader("Content-Type", "text/html");
                xmlHttp.send('');
                return xmlHttp.getResponseHeader("Date");
            }
        }



// New to Site? 에서 호출
// 
        $('#goJoin').click(function goJoin() {
            firebase.auth().signOut().then(function() {
            console.log(email, 'is signed out')
                // Sign-out successful.
            }).catch(function(error) {
                // An error happened.
            });
            location.href = "./register.html";
        });

        $(function() {
            var email = getQueryParam("email");
            if (email == false) {

            } else {
                $("#email").val(email);
                // Materialize.toast("If an ID exists, you will receive a password reset email. If you have not receive the mail, please check your ID", 4000)
            }
        });


        function getQueryParam(param) {
            var result = window.location.search.match(
                new RegExp("(\\?|&)" + param + "(\\[\\])?=([^&]*)"));
            return result ? result[3] : false;
        }


//로그인 버튼에서 호출

        $('#login').click(function Login() {
        
            var email = $('#email').val();
            var password = $('#password').val();

            firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(email, 'is logged in')


                if (errorCode == "auth/user-not-found" ||
                    errorCode == "auth/wrong-password") {
                    // Materialize.toast("The password is invalid for the given email.", 2000)
                } else {
                    // Materialize.toast(errorMessage, 2000)
                }

                //    Materialize.toast('I am a toast', 2000,'',function(){alert('Your toast was dismissed')})">Toast!</a>
            });
        });




        function FindPassword() {
            var auth = firebase.auth();
            var emailAddress = $("#icon_id").val();

            auth.sendPasswordResetEmail(emailAddress).then(function() {
                location.href = "./login.html?email="+emailAddress;
            }).catch(function(error) {
                location.href = "./login.html?email="+emailAddress;
            });
        }


//로그인한 경우에는 logout으로 표시하고,
//로그아웃한 경우에는 login으로 표시한다.

        // firebase.auth().onAuthStateChanged(function(user) {
        //     if (user) {
        //         boollogin = true;
        //         $("#login_status").html("Logout");
        //         // $("#join_delete").html("Delete Account");
        //     } else {
        //         boollogin = false;
        //         $("#login_status").html("Login");
        //         // $("#join_delete").html("Join");
        //     }
        // });

        $('#login_status').click(function LoginLogout(){
            if (boollogin) {
                firebase.auth().signOut().then(function() {
                console.log(email, 'is logged out')
                Materialize.toast('Logged Out!', 2000);
                // $( "#dialog-message" ).dialog({
                //     modal: true,
                //     buttons: {
                //         Ok: function() {
                //         $( this ).dialog( "close" );
                //         }
                //     }
                // });
                location.href = "./login.html";
                }).catch(function(error) {
                    // An error happened.
                });
            } else {
                // TO DO : goto login
                console.log('else routine')
                location.href = "./login.html";
            }
        });

        // function JoinDelete() {
        //     if (boollogin) {
        //         location.href = "./deleteuser.html";
        //     } else {
        //         // TO DO : goto join
        //         location.href = "./login.html";
        //     }
        // }