const auth = firebase.auth();

// get elements
const txtEmail = document.getElementById('txtEmail');
const txtPassword = document.getElementById('txtPassword');
const btnLogin = document.getElementById('btnLogin');
const btnSignUp = document.getElementById('btnSignUp');
const btnLogout = document.getElementById('btnLogout');


btnLogin.addEventListener('click', e => {
    //get email and pw
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();

    const promise = auth.signInWithEmailAndPassword(email, pass);

    console.log(promise.value);
    promise.catch(e => console.log(e.message));
    console.log('Logged in!', email);
    console.log('< 1 >');

});

// login page와 index 페이지 중에 어디로 갈 것인가를 결정
// firebase.auth().onAuthStateChanged(function(user) {
//     if (user) {
//         // User is signed in.
//         window.open('./profile.html', '_self', false);
//     } else {
//         // No user is signed in.
//         window.open('./login.html', '_self', false);
//     };
// });

btnSignUp.addEventListener('click', e => {
    //get email and pw
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();
    //sign up a new account 
    const promise = auth.createUserWithEmailAndPassword(email, pass);
    // 인증 관련 event가 catch되면, 그 event의 메시지를 출력한다
    console.log('< 2 >');
    promise.catch(e => console.log(e.message));
    console.log('New Account created!', email);

});

btnLogout.addEventListener('click', e => {
    // firebase.auth().signOut();
    const promise = auth.signOut();
    console.log('Logged out!', email);

    window.open('./login.html', '_self', false);

});