    var myUser;
        var uid;
        var balanceSheetRef;
        var balance_sheet = [];
        var boollogin = false;

// 유저의 상태가 달라지면 이 함수가 작동을 한다
// login을 logout으로 변경하거나, logout을 login으로 변경시킨다
// boollogin = false로 설정한 후, 로그인하게 되면 true로 바꾸고, login 표시도 logout으로 바꾼다
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                myUser = user;
                uid = user.uid;
                boollogin = true;
                // logout 표시를 login으로 수정함
                $("#login_logout").html("Logout");
                $("#join_delete").html("Delete Account");

                balanceSheetRef = firebase.database().ref('/users/' + uid + "/" + 'balance_sheet');
                eventCardLoad();
                initailizeComponent();
            } else {
                boollogin = false;
                $("#login_logout").html("Login");
                $("#join_delete").html("Join");

                Materialize.toast("Login first", 1000);
                location.href = "./login.html";
            }
        });

// card를 만들어서 web page(html)상에 표시하도록 한다
        function eventCardLoad() {
            $('#load').show();
            $("#event_list").empty();
            $('#event_list').append('<a class="modal-trigger" href="#modal1">' +
                '       <div class="col s12 m6">' +
                '          <div class="card blue-grey darken-1 waves-effect waves-light">' +
                '             <div class="card-content white-text">' +
                '                <span class="card-title">Create New Sheet</span>' +
                '               <i class="material-icons center-big-icon">add_circle_outline</i>' +
                '          </div>' +
                '     </div>' +
                '</div>' +
                '</a>');

            //   firebase DB에서 데이터 가져 옴  (테스트해 본 코드)
            // once는 한 번만 가져온다는 것임
                console.log(uid);
                var userRef = firebase.database().ref('/users/' + uid);
                
            userRef.once('value', function(abc) {
                var email2 = abc.val().email;
                console.log(email2);
            });


            // 스냅샷으로 가져와서 다시 구분한다. snapshot은 변수
            // 그 아래 tree의 데이터는 또 다시 forEach 문을 써서 가져온다.

            balanceSheetRef.once('value', function(snapshot) {
                snapshot.forEach(function(childSnapshot) {
                    var key = childSnapshot.key;
                    var data = childSnapshot.val();
                    CARD_HTML_APPEND(key, data);
                });
                $('#load').hide();
                //$('#modal1').modal('open');
            });
        }

        function initailizeComponent() {
            $('.modal').modal();
            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 2, // Creates a dropdown of 2 years to control year,
                today: 'Today',
                clear: 'Clear',
                close: 'Ok',
                closeOnSelect: false // Close upon selecting a date,
            });
        }

//   무슨 의미일까?
        function clickCard(key) {
            location.href = "./balance_sheet_item.html?eventId=" + key;
            //-L8jwIbLTn2864tVEGvY
        }

////// 함수: 웹에서 2가지 데이터를 가져 와서 DB에 쓴다. 

        function createNewEvent() {
            // 웹에서 라벨의 내용을 아이디로 가져온다. jquery로 사용
            var eventName = $("#event_name").val();
        
            var eventDescription = $("#event_description").val();

            var today = new Date();
            var options = {
                day: "numeric",
                month: "long",
                year: "numeric"

            };
            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];


            //  data 쓰는 방법
            // balanceSheetRef 하위에 2개 데이터를 쓴다.
            // balanceSheetRef.push()는 바로 아래 key를 의미하고, 이 key를 newEventRef로 넣고,
            // newEventRef.set 은 newEventRef key 하단을 모두 지우고 새로운 데이터로 다시 쓴다.
            var todayString = today.getDate() + " " + monthNames[today.getMonth()] + ", " + today.getFullYear();
            var newEventRef = balanceSheetRef.push();
            newEventRef.set({
                content: eventDescription,
                writeTime: todayString,
                title: eventName
            });
            
            // update를 쓰지 않고 set으로 원하는 필드 값만 변경하는 방법
            // 보통 set은 key 아래의 모든 데이터를 지우고 새로 저장하는 방식인데
            // 위 코드에서 newEventRef 하단 데이터 중에서 content 필드 내용만 새로 업데이트하려면 아래와 같이
            //     newEventRef.child ("필드명").set({ 저장하고자 하는 값을 가진 변수 })   
            //  을 사용한다.
            // 
            //            newEventRef.child("content").set({
            //                eventDescription,
            //            });

            eventCardLoad();
        }

// balanceSheetRef 하단 내용을 모두 삭제
        function removeCardEvent(key) {
            if (confirm("Are you sure you want to delete it?")) {
                var thisEventRef = balanceSheetRef.child(key);
                thisEventRef.remove();
                eventCardLoad();
            }
        }

//  내용 해석해 보자....
        function CARD_HTML_APPEND(key, data) {
            $('#event_list').append(
                '<div class="col s12 m6">' +
                '<div class="card blue-grey darken-1 waves-effect waves-light">' +
                '<div class="card-content white-text" onClick="clickCard(\'' + key + '\')">' +
                '<span class="card-title">' + data.title + '</span>' +
                '<p>' + data.content + '</p>' +
                '<p align="right">' + data.writeTime + '</p>' +
                '</div>' +
                '<p class="card-custom-bottom" onclick="removeCardEvent(\'' + key + '\')">delete</p>' +
                '</div>' +
                '</div>');
        }

//  로그인된 상태(boollogin = true)라면 main page로 이동, 
// 그렇지 않으면 그대로 login page 유지
        function LoginLogout() {
            if (boollogin) {
                firebase.auth().signOut().then(function() {
                    location.href = "./main.html";
                }).catch(function(error) {
                    // An error happened.
                });
            } else {
                // TO DO : goto login
                location.href = "./login.html";
            }

        }
// 회원가입 내용을 삭제하는 내용
// joinDelete 함수를 실행하는 event를 만든 경우,
// 로그인되어 있는 상태라면 deleteuser.html 페이지로 이동
        function JoinDelete() {
            if (boollogin) {
                location.href = "./deleteuser.html";
            } else {
                // TO DO : goto join
                location.href = "./join.html";
            }
        }