$(document).ready(function($) {
    firebase.auth().onAuthStateChanged(function(user) {

        var cu = window.location.href;
        var n1 = cu.indexOf('login');
        if (user) {
            if (n1 > 1) {
                window.open('./index.html', '_self', false);
            }
        } else {
            if (n1 < 1) {
                window.open('./login.html', '_self', false);
            }
        }

    });
});



function logout() {
    firebase.auth().signOut().then(function() {}, function(error) {});
};

function login() {
    firebase.auth().createUserWithEmailAndPassword($("#email").val(), $("#password").val()).then(function(result) {

    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        // var errorMessage = error.message;
        alert(errorCode);
        // ...
    });
}