// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/firestore");

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyBjnMInM9HB8TtdiwPSNFfpHpsXR1xU4uA",
    authDomain: "jahyun-test1.firebaseapp.com",
    databaseURL: "https://jahyun-test1.firebaseio.com",
    projectId: "jahyun-test1",
    storageBucket: "jahyun-test1.appspot.com",
    messagingSenderId: "87750603983",
    appId: "1:87750603983:web:e1ed366b7d931bf00133ba"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


// login page와 index 페이지 중에 어디로 갈 것인가를 결정
$(document).ready(function($) {
    firebase.auth().onAuthStateChanged(function(user) {

        // 현재 location의 url에 login 이름을 포함된 경우라면...
        var cu = window.location.href;
        var n1 = cu.indexOf('login');

        // user로 인증되면 index.htm로... 아니면 다시 그대로
        if (user) {
            if (n1 > 1) {
                window.open('./index.html', '_self', false);
            }
        } else {
            if (n1 < 1) {
                window.open('./login.html', '_self', false);
            }
        }

    });
});

$(document).ready(function($) {
    firebase.auth().onAuthStateChanged(function(user) {

        if (user) { console.log('log', user); } else {
            console.log('err', 'not yet')
        }

    });
});


function logout() {
    firebase.auth().signOut().then(function() {}, function(error) {});
};

function login() {
    firebase.auth().createUserWithEmailAndPassword($("#email").val(), $("#password").val()).then(function(result) {

    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        // var errorMessage = error.message;
        alert(errorCode);
        // ...
    });
}