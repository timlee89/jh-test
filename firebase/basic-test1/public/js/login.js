/**  Description: 
// login.html에서 사용
// input ID:  txtEmail, txtPassword
// 호출 함수: Login(),  goSignup()
*/
const auth = firebase.auth();
var boollogin = false;



// email 형식 check
$(function() {
    var email = getQueryParam("email");
    if (email == false) {

    } else {
        $("#txtEmail").val(email);
        Materialize.toast("If an ID exists, you will receive a password reset email. If you have not receive the mail, please check your ID", 4000)
    }
});


function getQueryParam(param) {
    var result = window.location.search.match(
        new RegExp("(\\?|&)" + param + "(\\[\\])?=([^&]*)"));
    return result ? result[3] : false;
}

//로그인 버튼에서 호출

function Login() {
    var email = $('#txtEmail').val();
    var password = $('#txtPassword').val();

    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;

        if (errorCode == "auth/user-not-found" ||
            errorCode == "auth/wrong-password") {
            Materialize.toast("The password is invalid for the given email.", 2000)
        } else {
            Materialize.toast(errorMessage, 2000)
        }

        //                Materialize.toast('I am a toast', 2000,'',function(){alert('Your toast was dismissed')})">Toast!</a>
    });
}

firebase.auth().onAuthStateChanged(function(user) {
    if (user = auth.currentUser) {   
        location.href = "./main.html";
    } else {}
});

function goSignup() {
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
    }).catch(function(error) {
        // An error happened.
    });
    location.href = "./signup.html";
}

