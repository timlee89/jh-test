var express = require("express");
var passport = require("passport");

var User = require("./models/user");
var router = express.Router();

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        req.flash("info", "You must be logged in to see this page.");
        res.redirect("/login");
    }
}

// 템플릿의 변수들을 설정
// 요청한 유저와 현재 유저가 같은지 비교 확인
router.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    res.locals.errors = req.flash("error");
    res.locals.infos = req.flash("info");
    next();
});

// 사용자 collection을 query하고, 가장 최근 사용자를 먼저 반환 
router.get("/", function(req, res, next) {
    User.find()
        .sort({ createdAt: "descending" })
        .exec(function(err, users) {
            if (err) { return next(err); }
            res.render("index", { users: users });
        });
});

// app.js에서 views 폴더를 정의했으므로...
// "/login"을 방문할 때 views 폴더에 있는 login.ejs 파일을 렌더링한다
router.get("/login", function(req, res) {
    res.render("login");
});
// login.ejs 페이지에서 받은 변수로 로그인 인증을 진행한다
router.post("/login", passport.authenticate("login", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true
}));

// logout 라우트
//  <li><a href="/logout">Log out</a></li> 에서 "/logout"을 호출하면 이 라우트를 실행한다.
router.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/");
});

// 신규등록 sign up 라우트 
// signup.ejs 페이지를 우선 렌더링한 후에...
router.get("/signup", function(req, res) {
    res.render("signup");
});

// signup.ejs에서 받은 변수를 가지고 신규등록 절차를 진행한다
router.post("/signup", function(req, res, next) {

    var username = req.body.username;
    var password = req.body.password;

    User.findOne({ username: username }, function(err, user) {

        if (err) { return next(err); }
        if (user) {
            req.flash("error", "User already exists");
            return res.redirect("/signup");
        }

        var newUser = new User({
            username: username,
            password: password
        });
        newUser.save(next);

    });
}, passport.authenticate("login", {
    successRedirect: "/",
    failureRedirect: "/signup",
    failureFlash: true
}));

// profile 라우트 
router.get("/users/:username", function(req, res, next) {
    User.findOne({ username: req.params.username }, function(err, user) {
        if (err) { return next(err); }
        if (!user) { return next(404); }
        res.render("profile", { user: user });
    });
});

// edit 라우트 
// edit.ejs 페이지를 렌더링한 후에...
router.get("/edit", ensureAuthenticated, function(req, res) {
    res.render("edit");
});
// 받은 데이터로 displayName, bio 등의 정보를 업데이트한다
router.post("/edit", ensureAuthenticated, function(req, res, next) {
    req.user.displayName = req.body.displayname;
    req.user.bio = req.body.bio;
    req.user.save(function(err) {
        if (err) {
            next(err);
            return;
        }
        req.flash("info", "Profile updated!");
        res.redirect("/edit");
    });
});

module.exports = router;