var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var express = require("express");
var flash = require("connect-flash");
var mongoose = require("mongoose");
var passport = require("passport");
var path = require("path");
var session = require("express-session");


var setUpPassport = require("./setuppassport");
var routes = require("./routes"); // 라우트 내용이 있는 routes.js 내용을 가져온다


var app = express();
// 테스트 데이터베이스로서 MongoDB servr에 연결한다
mongoose.connect("mongodb://localhost:27017/test");
setUpPassport(); //passport 설정 요청 

app.set("port", process.env.PORT || 3000);



app.set("views", path.join(__dirname, "views")); // express에 views 폴더가 어딘지를 알린다
app.set("view engine", "ejs"); // ejs로 끝나는 파일은 ejs패키지로 렌더링하도록 EJS에 알린다

app.use(express.static(path.join(__dirname, "public")));



// passport에 대한 middleware 설정 ---> Start
// 4개의 미들웨어를 사용한다
app.use(bodyParser.urlencoded({ extended: false }));
// bodyparser의 extended: false로 설정하면 더 단순하고 안전하게 분석한다
app.use(cookieParser());

app.use(session({
    secret: "LUp$Dg?,I#i&owP3=9su+OB%`JgL4muLF5YJ~{;t",
    // secret: 임의의 문자를 설정해서 각 세션이 클라이언트에서 암호화되로고 한다
    resave: true,
    saveUninitialized: true
}));
app.use(flash());

app.use(passport.initialize()); // passport 모듈 초기화
app.use(passport.session()); // passport 세션 처리

// passport에 대한 middleware 설정 ---> END


// 다양한 라우트 미들웨어 사용
app.use(routes);


app.listen(app.get("port"), function() {
    console.log("Server started on port " + app.get("port"));
});