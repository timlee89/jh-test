// http이라는 모듈이 필요하다는 의미
// http module을 http라는 변수에 담는다는 의미
const http = require('http');
// const ; 한번 설정하면 바뀌지 않는다는 의미
// var ; 변수 값이 바뀔 수 있다는 의미
/** Webserver를 생성해서 대기시키는 코드임 */

// 이 파일을 실행하면 server가 대기 상태가 
// http://127.0.0.1:1337/ 로 접속하면 아래 Hello World가 보인다.
// web server를 생성해서 그 서버에 hello world라는 텍스트를 전송한 것이다.

const hostname = '127.0.0.1';
const port = 1337;

// server를 만들어서, 그 서버가 hostname의 port 1337을 listening(대기)하게 한다.
// 여기로 접속하는 사용자에게 ...이렇게 응답한다는 의미임

/**  표현 방식 1 */

// http.createServer((req, res) => {
//     res.writeHead(200, { 'Content-Type': 'text/plain' });
//     res.end('Hello World\n');
// }).listen(port, hostname, () => {
//     console.log(`Server running at http://${hostname}:${port}/`);
// });


// /**  표현 방식 2 = 위 표현방식1과 동일한 코드임 */

//  webserver를 생성하였고
// 아래 대기하고 있는 서버로 request가 들어오면 response로서 다음과 같은 명령을 진행한다는 의미
var server = http.createServer(function(req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello World\n');
});
// hostname:port 주소로 들어오는 것을 대기하고, 
// 대기상태로 들어갈 때 실행할 내용을 callback 함수에 넣는다
server.listen(port, hostname, function() {
    console.log(`Server running at http://${hostname}:${port}/`);
});