var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs')
var app = express();
app.use(bodyParser.urlencoded({ extended: false }))

app.locals.pretty = true;
app.set('views', './views_file'); //  views_file 폴더에 파일넣는다
app.set('view engine', 'jade'); // view engine으로 jade 사용
app.get('/topic/new', function(req, res) {
    res.render('new');
})

app.post('/topic', function(req, res) {
    console.log('1')
    res.send('Hi, post, ', '+req.body.title');
})

app.listen(3000, function() {
    console.log('connected to port 3000!')
})